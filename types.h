#ifndef TYPES_H_INCLUDED
#define TYPES_H_INCLUDED

typedef struct no No;

struct no{
    int valor;
    struct no *prx;
};


No * criaLista();
int listaVazia(No *l);
No * insereNoInicio(No * l, int v);
//#1
No * incializaListaDeArquivoCSV(No * l, char *arq);
//#2
void imprimeLista(No *l);
//#3
No * removeElementoValor(No * l, int valor);
//#4
No * removeElementoPos(No * l, int pos);
//#5
No * zeraLista(No * l);
//#6
No * insereNoMeio(No * l, float v, int pos);
//#7
No * removeDuplicatas(No * l);
//#8
No * inverteLista(No * l);
//#9
No menorElemento(No * l);
//#10
No buscaElemento(No *l, int v);
//#11
int verificaCrescente(No * l);
//#12
int profundidadeElemento(No *l, int v);
//#13
int tamanhoLista(No *l);
//#14
int alturaElemento(No * l, int v);
//#15
int maiorNrOcorrencias(No *);
//#16
int difMaiorMenor(No *l);

#endif // TYPES_H_INCLUDED
