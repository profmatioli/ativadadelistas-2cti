#include <stdio.h>
#include "types.h"

No * criaLista(){
    return NULL;
}

int listaVazia(No *l){
    return l==NULL;
}

No * insereNoInicio(No * l, int v){
    No * novo;
    novo = (No *) malloc(sizeof(No));
    if(novo!=NULL){
        novo->valor = v;
        novo->prx = l;
        l = novo;
    }
    return l;
}

void imprimeLista(No *l){
    No * p;
    if(listaVazia(l)){
        printf("Lista esta vazia!!!\n");
        return;
    }
    for(p=l; p!=NULL; p=p->prx){
        printf("%d; ",p->valor);
    }
}
